# Potion Monsters NFT

<img src="https://potionmonsters.com/images/the-roadmap-monsters.png" alt="potionmonsters">

## PSSST!
## STAY QUIET FOR A SECOND

Can you hear the whistling and swishing sounds of clanging weapons?<br>
Yeah! That’s us!<br>
We are little, cute, fluffy colorful monsters that only want to posses the cosmic energy to deploy our elements power and prove who’s the foremost.<br>
We are peaceful nation that live in harmony except the moments of fierce fights! You can help us with our arguments.<br>
What do you think water or earth, fire or air is the all mighty element?<br>
We can show you but do you have the cosmic energy in you?<br>
On the battle field we compete each other to show which element is the most powerful, but what we need is the cosmic energy that fill the potions to fully reveal the power of monster elements.


## NFT TURN-BASED BROWSER GAME ON CARDANO BLOCKCHAIN

NFT Turn-Based browser game where your monsters will use potions (skills) and together with a combination of the elements they possess (water, fire, earth and air) will deal damage to their opponents.<br>
<b style="color:aqua">Monsters:</b> Twenty monsters born with the elements they are marked with. Each one can possess one or all four elements. Тhe more elements the monster has, the stronger it is.<br>
<b style="color:aqua">Elements:</b> The four elements give an additional crushing bonus to the potions (skills) that the monsters will be fighting with. The elements of the monster and the potions must have the same element to have bonus destruction.<br>
<b style="color:aqua">Potions:</b> The magic potions are the source of monsters' power and each potion holds one of the four elements that give monsters different skills.<br>
<b style="color:aqua">Skinns:</b> There are four skin types that give the monster bonus stamina:<br>
Light, Medium, Heavy and Void. Тhe rarer the monster is, the greater the bonus of the skin is.<br>
<b style="color:aqua">Cosmic energy:</b> Powerful energy that gives you fuel to use your skills.

## FOLLOW US!
<div>
                <ion-icon name="logo-twitter" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://twitter.com/potionmonsters" target="_blank" style="color:aqua">Twitter: https://twitter.com/potionmonsters </a>
                <br><ion-icon name="logo-facebook" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://www.facebook.com/potionmonsters" target="_blank" style="color:aqua">Facebook: https://www.facebook.com/potionmonsters </a>
                <br><ion-icon name="logo-instagram" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://www.instagram.com/potionmonsters/" target="_blank" style="color:aqua">Instagram: http://instagram.com/potionmonsters </a>
                <br><ion-icon name="logo-pinterest" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://pinterest.com/potionmonsters" target="_blank" style="color:aqua">Pinterest: https://pinterest.com/potionmonsters </a>
                <br><ion-icon name="logo-discord" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://discord.gg/cYNZxkNVSS" target="_blank" style="color:aqua">Discord: https://discord.gg/cYNZxkNVSS </a>
                <br><ion-icon name="logo-gitlab" size="large"></ion-icon>&nbsp;&nbsp;<a href="https://gitlab.com/vaitlspaak/potionmonsters" target="_blank" style="color:aqua">GitLab: https://gitlab.com/vaitlspaak/potionmonsters </a>
                <br><ion-icon name="mail-outline" size="large"></ion-icon>&nbsp;&nbsp;<a href="mailto:contact@potionmonsters.com" style="color:aqua">Email:contact@potionmonsters.com</a>
</div>